package testRunner;


import java.io.IOException;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterGroups;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import helpers.GenerateReport;
import helpers.propertieshelpers;



@RunWith(Cucumber.class)
@CucumberOptions(
		features = "classpath:features",
		glue = { "step_definitions"},
		format = {"json:target/cucumber-parallel/safari.json", "html:target/cucumber-parallel/safari.html", "pretty"}
		//format = {"json:target/cucumber-reports/cucumber.json","html:target/cucumber-reports"}
		//tags = {"@debug"}
)
public class SafariRunner {
	

	
	@BeforeClass()
	public static void onceExecutedBeforeAll() throws IOException {
		propertieshelpers.loadPropertiesFile("integration");
		propertieshelpers.LoadBrowserFile("safari.properties");
		
	}
	
	


}
