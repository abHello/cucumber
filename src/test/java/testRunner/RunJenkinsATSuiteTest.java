package testRunner;


import cucumber.api.junit.Cucumber;
import helpers.propertieshelpers;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;




@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        glue = { "step_definitions"},
        format = {"json:target/cucumber-reports/cucumber.json","html:target/cucumber-reports"},
        tags = {"@test"}
)
public class RunJenkinsATSuiteTest{
    public  static String  environment;
    @BeforeClass()

    public static void onceExecutedBeforeAll() {

        environment = System.getProperty("env");
        if (environment == null) environment = "qa";
        propertieshelpers.loadPropertiesFile(environment);
    }
}
