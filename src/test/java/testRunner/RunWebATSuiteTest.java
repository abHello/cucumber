package testRunner;


import org.junit.BeforeClass;
import org.junit.runner.RunWith;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import helpers.propertieshelpers;


@RunWith(Cucumber.class)
@CucumberOptions(

		features = "classpath:features",
		glue = { "step_definitions"},
		format = {"json:target/cucumber-reports/cucumber.json","json:target/cucumber-parallel/firefox.json","json:target/cucumber-parallel/chrome.json","json:target/cucumber-parallel/safari.json","html:target/cucumber-reports"},
		tags = {"@test"}
)
public class RunWebATSuiteTest{

	public  static String  environment;
	@BeforeClass()
	public static void onceExecutedBeforeAll() {

		environment = System.getProperty("env");
		if (environment == null) environment = "integration";
		propertieshelpers.loadPropertiesFile(environment);
	}
}