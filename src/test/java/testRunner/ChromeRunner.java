package testRunner;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import helpers.propertieshelpers;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;



@RunWith(Cucumber.class)
@CucumberOptions(
		strict = true,
		features = "classpath:features",
		glue = { "step_definitions"},
		format = {"json:target/cucumber-parallel/chrome.json", "html:target/cucumber-parallel/chrome.html", "pretty"},
	    monochrome = false
		//format = {"json:target/cucumber-reports/cucumber.json","html:target/cucumber-reports"}
		//tags = {"@debug"}
)
public class ChromeRunner {
	


	
	@BeforeClass()
	public static void onceExecutedBeforeAll() throws IOException {
		
		propertieshelpers.loadPropertiesFile("integration");
		propertieshelpers.LoadBrowserFile("chrome.properties");

	}


}
