package step_definitions;

import static org.testng.Assert.assertTrue;

import cucumber.api.PendingException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import helpers.propertieshelpers;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.PaymentPage;
import pageobjects.OrderConfirmedPage;



public class Payment  {
    public WebDriver driver;

    public Payment() {
        driver = Hooks.driver;
    }
    @Given("^un client authentifié sur \"(.*?)\" ayant des articles dans son panier et ayant fait son choix du mode de livraison$")
    public void un_client_authentifié_ayant_des_articles(String url) throws Throwable {
        PageFactory.initElements(driver, Delivery.class);
        Delivery de=new Delivery();
        de.un_client_authentifié_sur_et_ayant_des_articles_dans_son_panier(url);
        de.je_souhaite_finaliser_ma_commande();
    }


    @Given("^un client authentifié sur \"(.*?)\" se situant sur la page de paiement et ayant accepter les CGV$")
    public void un_client_authentifié_sur_se_situant_sur_la_page_de_paiement_et_ayant_accepter_les_CGV(String url) throws Throwable {
        PageFactory.initElements(driver, Delivery.class);
        Delivery de=new Delivery();
        Payment Pay= new Payment();
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        de.un_client_authentifié_sur_et_ayant_des_articles_dans_son_panier(url);
        de.je_souhaite_finaliser_ma_commande();
        Pay.je_souhaite_effectuer_le_paiment_de_ma_commande();


    }
    @When("^je souhaite effectuer le paiment de ma commande$")
    public void je_souhaite_effectuer_le_paiment_de_ma_commande() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.PaymentAction();
    }

    @When("^je souhaite effectuer le paiment de ma commande avec ma carte bancaire déjà enregistrée$")
    public void je_souhaite_effectuer_le_paiment_de_ma_commande_avec_ma_carte_bancaire_déjà_enregistrée() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.PaymentChoiceAndDo();
        PaymentPage.insertCardNumber(propertieshelpers.getProperty("cardnumber"),propertieshelpers.getProperty("cardcsv"));
    }

    @When("^je souhaite effectuer le paiment de ma commande par virement$")
    public void je_souhaite_effectuer_le_paiment_de_ma_commande_par_virement() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.PaymentTransfer();
           }

    @When("^je souhaite effectuer le paiment de ma commande par cheque$")
    public void je_souhaite_effectuer_le_paiment_de_ma_commande_par_cheque() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.PaymentBankCheck();
    }

    @When("^je souhaite effectuer le paiment de ma commande par paypal$")
    public void je_souhaite_effectuer_le_paiment_de_ma_commande_par_Paypal() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.PaymentPaypal();
    }

    @Then("^la page de paiement s’affiche avec l'adresse de facturation et il est possible de choisir un mode de paiement$")
    public void la_page_de_paiement_s_affiche_avec_l_adresse_de_facturation_et_il_est_possible_de_choisir_un_mode_de_paiement() throws Throwable {
        PageFactory.initElements(driver, PaymentPage.class);
        PaymentPage PaymentPage=new PaymentPage(driver);
        PaymentPage.AssertPaymentPage();
    }

    @Then("^la commande est passée$")
    public void la_commande_est_passée() throws Throwable {
        PageFactory.initElements(driver, OrderConfirmedPage.class);
        OrderConfirmedPage order=new OrderConfirmedPage(driver);
        order.AssertOrderConfirmed();
    }

}
