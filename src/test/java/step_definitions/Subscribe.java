package step_definitions;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.Handles;
import helpers.propertieshelpers;
import pageobjects.AddToBasketPage;
import pageobjects.DeliveryPage;
import pageobjects.HomePage;
import pageobjects.LoginPage;
import pageobjects.RajaMailBoxPage;
import pageobjects.SubscribePage;

public class Subscribe {
    public WebDriver driver;
    HomePage homeP;
    SubscribePage subscribeP;
    RajaMailBoxPage rajaMail;
    AddToBasketPage addTB;
    DeliveryPage delivery;
    LoginPage lp;
    Handles randomEmail=new Handles();
    public String useremail=randomEmail.RandomEmail();

    public Subscribe() {
        driver = Hooks.driver;
    }

    @Given("^Un utilisateur se rendant sur le site Raja \"(.*?)\"$")
    public void un_utilisateur_se_rendant_sur_le_site_Raja(String url)  {
    	PageFactory.initElements(driver,HomePage.class);
    	homeP=new HomePage(driver);
    	homeP.getToURL(propertieshelpers.getProperty(url));
    }
    
    @Given("^Un utilisateur se rendant sur le site Raja \"(.*?)\" et ayant des articles dans son panier$")
    public void creer_compte_tunnel_achat(String url) throws InterruptedException {
    	PageFactory.initElements(driver,AddToBasketPage.class);
    	PageFactory.initElements(driver,DeliveryPage.class);
    	driver.get(propertieshelpers.getProperty(url));
    	addTB=new AddToBasketPage(driver);
    	// speceficité chrome mac os
    	Thread.sleep(1000);
    	addTB.add_article_to_basket(propertieshelpers.getProperty("reference_produit"));
    	delivery=new DeliveryPage(driver);
    	delivery.LivraisonAction();
    	
    }
    
    @When("^l'utilisateur choisit de créer son compte \"(.*?)\", \"(.*?)\" pour valider son panier$")
    public void utilisateur_cree_compte_pour_valider_panier(String organisation, String postalCode) throws InterruptedException {
    	PageFactory.initElements(driver,SubscribePage.class);
    	PageFactory.initElements(driver,LoginPage.class);
    	lp=new LoginPage(driver);
    	lp.CreateAccountAccess();  
    	subscribeP=new SubscribePage(driver);
    	subscribeP.register(useremail, propertieshelpers.getProperty(organisation), propertieshelpers.getProperty(postalCode));
    }

    @When("^l'utilisateur choisit de créer son compte \"(.*?)\", \"(.*?)\"$")
    public void l_utilisateur_choisit_de_créer_son_compte(String organisation, String postalCode) throws InterruptedException {
        PageFactory.initElements(driver,HomePage.class);
        PageFactory.initElements(driver,SubscribePage.class);
        homeP=new HomePage(driver);
        subscribeP=new SubscribePage(driver);
        //specificité chrome mac os
        Thread.sleep(1000);
        homeP.RegisterPageAccess();
        subscribeP.register(useremail, propertieshelpers.getProperty(organisation), propertieshelpers.getProperty(postalCode));
    }

    @Then("^le compte est crée$")
    public void le_compte_est_crée() {
        PageFactory.initElements(driver,SubscribePage.class);
        subscribeP.assert_account_is_created();
    }

    @Then("^un email de confirmation est envoyé au client$")
    public void un_email_de_confirmation_est_envoyé_au_client() throws Exception {
        // to implement
    }

    @Then("^un email de confirmation est envoyé au CRC$")
    public void un_email_de_confirmation_est_envoyé_au_CRC() {
        // to implement

    }


}