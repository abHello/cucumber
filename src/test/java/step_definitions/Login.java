package step_definitions;


import static org.testng.Assert.assertTrue;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.propertieshelpers;
import pageobjects.LoginPage;



public class Login {

	public WebDriver driver;

	public static String Email;
	public Login() {
		driver = Hooks.driver;

	}
	
	@Given("^Un user se rendant sur le site d'authentification de Raja \"([^\"]*)\"$")
	public void open_login_raja_website(String url) {
		//ChromeDriverManager.getInstance().setup();
		driver.get(propertieshelpers.getProperty(url));
	}
	
	@When("^Je me connecte avec username \"([^\"]*)\" et password \"([^\"]*)\"$")
	public void login(String email, String password) {	
		PageFactory.initElements(driver, LoginPage.class);
		LoginPage lp=new LoginPage(driver);
		lp.SignUpAccess();
		lp.LoginAction(propertieshelpers.getProperty(email), propertieshelpers.getProperty(password));
	}
	
	@When("^Je rentre le compte \"([^\"]*)\"$")
	public void passwordRecovery(String email) {

		PageFactory.initElements(driver, LoginPage.class);
		LoginPage lp=new LoginPage(driver);
		lp.SignUpAccess();
		lp.PasswordRecoveryAction(propertieshelpers.getProperty(email));
		Email=propertieshelpers.getProperty(email);
	}

	
	@Then("je suis connecté")
	public void im_logged_in() {
	    	assertTrue(driver.getCurrentUrl().contains("SignIn=true"), "Connexion échouée.");
	}
	
	@Then("je ne suis pas connecté")
	public void im_not_logged_in() {
		assertTrue(driver.getCurrentUrl().contains("ProcessLogin"), "Connexion au site RAJA avec faux identifiants.");
	}
	
	@Then("Une modal d'erreur s'affiche")
	public void errorSent() {
		PageFactory.initElements(driver, LoginPage.class);
		LoginPage lp = new LoginPage(driver);
		lp.AssertWarning();
	}
	
	@Then("Une modal d'envoie s'affiche")
	public void messageSent() {
		PageFactory.initElements(driver, LoginPage.class);
		LoginPage lp = new LoginPage(driver);
		lp.GrantedAccess();
	}

}