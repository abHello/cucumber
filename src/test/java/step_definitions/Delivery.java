package step_definitions;
import cucumber.api.PendingException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.propertieshelpers;
import pageobjects.AddToBasketPage;
import pageobjects.DeliveryPage;
import pageobjects.LoginPage;
import pageobjects.SubscribePage;


public class Delivery {

    public WebDriver driver;

    public Delivery() {
        driver = Hooks.driver;
    }

    @Given("^un client non authentifié sur \"(.*?)\" et ayant des articles dans son panier$")
    public void un_client_non_authentifié_sur_et_ayant_des_articles_dans_son_panier(String url) throws Throwable {
        driver.get(propertieshelpers.getProperty(url));
        PageFactory.initElements(driver, AddToBasketPage.class);
        PageFactory.initElements(driver, Delivery.class);
        AddToBasketPage addTB=new AddToBasketPage(driver);
        Thread.sleep(1000);
        addTB.order_by_reference();
        addTB.enter_product_reference("F");
        addTB.Select_product_from_list();
        addTB.add_product_to_basket();
        addTB.view_cart();
    }
    @Given("^un client authentifié sur \"(.*?)\" et ayant des articles dans son panier$")
    public void un_client_authentifié_sur_et_ayant_des_articles_dans_son_panier(String url) throws InterruptedException {
        driver.get(propertieshelpers.getProperty(url));
        PageFactory.initElements(driver, AddToBasketPage.class);
        PageFactory.initElements(driver, Delivery.class);
        PageFactory.initElements(driver, LoginPage.class);
        LoginPage lp=new LoginPage(driver);
        lp.SignInAccess();
        lp.LoginAction(propertieshelpers.getProperty("username.login.valid"), propertieshelpers.getProperty("username.password.valid"));
        AddToBasketPage addTB=new AddToBasketPage(driver);
        Thread.sleep(1000);
        addTB.order_by_reference();
        addTB.enter_product_reference("gt");
        addTB.Select_product_from_list();
        addTB.add_product_to_basket();
        addTB.view_cart();
    }

    @Given("^un client s'inscrit sur \"(.*?)\" et ayant des articles dans son panier et ayant fait son choix de mode de livraison$")
    public void un_client_sinscrit_sur_et_ayant_des_articles_dans_son_panier_et_ayant_fait_son_choix_de_mode_de_livraison(String url) throws Throwable {
        driver.get(propertieshelpers.getProperty(url));
        PageFactory.initElements(driver, SubscribePage.class);
        Delivery De=new Delivery();
        Subscribe Su=new Subscribe();
        AddToBasket Basket=new AddToBasket();
        Su.l_utilisateur_choisit_de_créer_son_compte("organisation","postalCode.fr");
        Basket.add_product_to_basket("reference_produit");
        De.je_souhaite_finaliser_ma_commande();
    }


    @When("^je souhaite finaliser ma commande$")
    public void je_souhaite_finaliser_ma_commande() throws Throwable {
        DeliveryPage Delivery=new DeliveryPage(driver);
        PageFactory.initElements(driver, DeliveryPage.class);
        Delivery.LivraisonAction();
    }

    @When("^je souhaite modifier l'adresse de livraison$")
    public void je_souhaite_modifier_l_adresse_de_livraison() throws Throwable {
        DeliveryPage Delivery=new DeliveryPage(driver);
        PageFactory.initElements(driver, DeliveryPage.class);
        Delivery.ModifyDelivry();
    }
    @Then("^la fenêtre d’authentification apparait proposant deux choix : l’authentification et l’inscription\\.$")
    public void la_fenêtre_d_authentification_apparait_proposant_deux_choix_l_authentification_et_l_inscription() throws Throwable {
        LoginPage Login=new LoginPage(driver);
        PageFactory.initElements(driver, LoginPage.class);
        Login.AssertPopUpLogin();
    }
    @Then("^la fenêtre de livraison apparait avec : les informations du client  le tableau récapitulatif des articles, et le choix du mode de livraison$")
    public void la_fenêtre_de_livraison_apparait()  {
        DeliveryPage Delivery=new DeliveryPage(driver);
        PageFactory.initElements(driver, DeliveryPage.class);
        Delivery.Assert_livraisonPage();

    }

    @Then("^la page est modifiée$")
    public void la_page_est_modifiée() throws Throwable {
        DeliveryPage Delivery=new DeliveryPage(driver);
        PageFactory.initElements(driver, DeliveryPage.class);
        Delivery.assertModify_Succes();
    }

}