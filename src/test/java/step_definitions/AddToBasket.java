package step_definitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helpers.propertieshelpers;
import pageobjects.AddToBasketPage;
import pageobjects.LoginPage;

public class AddToBasket {
	
	public WebDriver driver;
	AddToBasketPage addTB;
	LoginPage lp;
	
	public AddToBasket() {
		driver = Hooks.driver;
	}
	
	
	@Given("^Un utilisateur non authentifié se rendant dans la page de commande par référence \"([^\"]*)\"$")
	public void open_site_raja(String url)  {
		driver.get(propertieshelpers.getProperty(url));	
	}
	
	@Given("^Un utilisateur authentifié se rendant dans le site Raja \"(.*?)\"$")
	public void un_utilisateur_authentifié_se_rendant_dans_le_site_Raja(String url) throws InterruptedException {
		PageFactory.initElements(driver, LoginPage.class);
		driver.get(propertieshelpers.getProperty(url));	
		lp=new LoginPage(driver);
		//spécificité chrome mac
		Thread.sleep(1000);
		lp.SignInAccess();
		lp.LoginAction(propertieshelpers.getProperty("username.login.valid"), propertieshelpers.getProperty("username.password.valid"));
	}
	
	@When("^l'utilisateur ajoute un article \"([^\"]*)\" dans son panier$")
	public void add_product_to_basket(String ref_Prod) throws InterruptedException {
		PageFactory.initElements(driver, AddToBasketPage.class);
		addTB=new AddToBasketPage(driver);
		Thread.sleep(1000);
		addTB.order_by_reference();
		addTB.enter_product_reference(propertieshelpers.getProperty(ref_Prod));
		addTB.Select_product_from_list();
		addTB.add_product_to_basket();		
		addTB.view_cart();		
	}
	
	@Then("^l'article est présent dans son panier$")
	public void article_is_present() {
		PageFactory.initElements(driver, LoginPage.class);
		PageFactory.initElements(driver, AddToBasketPage.class);
		addTB=new AddToBasketPage(driver);
		addTB.verify_product_exist_in_basket();
		
	}

}
