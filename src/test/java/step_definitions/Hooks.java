package step_definitions;


import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class Hooks{
    public static WebDriver driver;

    public  static String  browserName, os, platformName, browser;
    public static final String URL = "http://hub-cloud.browserstack.com/wd/hub/";


    @Before
    /**
     * Delete all cookies at the start of each scenario to avoid
     * shared state between tests
     */
    public void openBrowser() throws IOException {

        System.out.println("Called openBrowser");
        os = (System.getProperty("os.name"));


        //************************************* Driver Manager ***************************************
        browserName = System.getProperty("browserName");
        if (browserName == null) browserName="chrome";
        System.out.println("browser is : "+browserName);
        if(browserName.equals("chrome")&&os.equals("Windows 10")||os.equals("Mac OS X")) {

            if (os.equalsIgnoreCase("windows")) System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            else if (os.equalsIgnoreCase("Mac OS X")) System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
            else	System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            driver = new ChromeDriver(options);
        }

        else if(browserName.equalsIgnoreCase("safari")) {
            if (os.equalsIgnoreCase("Mac OS X")) {
                SafariOptions options = new SafariOptions();
                driver = new SafariDriver(options);  }
        }

        else
            if (os.equalsIgnoreCase("LINUX")) {

                browserName = System.getProperty("browserName");
                platformName = System.getProperty("platformName");
                if (browserName == null) browserName = "chrome";
                if (platformName == null) platformName = "WINDOWS";
                URL browserStackUrl = new URL(URL);
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability("os", platformName);
                caps.setCapability("os_version", "10");
                caps.setCapability("browser", browserName);
                caps.setCapability("max_duration","300");
                caps.setCapability("browserstack.user", "pocraja1");
                caps.setCapability("browserstack.key", "KqzyefdVMJDJEsjxhy5H");
                driver = new RemoteWebDriver(browserStackUrl, caps);
            } else if (browserName.equals("IE")) {
                driver = new InternetExplorerDriver();
               
                if (os.equalsIgnoreCase("Mac OS X"))
                    System.setProperty("webdriver.ie.driver", "src/test/resources/drivers/geckodriver");
                else System.setProperty("webdriver.ie.driver", "path.exe");
            }
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }


    @After
    /**
     * Embed a screenshot in test report if test is marked as failed
     */
    public void embedScreenshot(Scenario scenario) {

        if(scenario.isFailed()) {
            try {
                scenario.write("Current Page URL is " + driver.getCurrentUrl());
                byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            } catch (WebDriverException somePlatformsDontSupportScreenshots) {
                System.err.println(somePlatformsDontSupportScreenshots.getMessage());
            }
        }
        driver.quit();

    }

}