package pageobjects;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import helpers.Log;


public class DeliveryPage extends BaseClass{

    public DeliveryPage(WebDriver driver) {
        super(driver);
    }

    //************************************************** WebElements **************************************************
    @FindBy(css="button[class='btns red arrowr']")
    public static WebElement debutOrder_button;

    @FindBy(css="a[class='btns bdrred arrowr m-r-10']")
    public static WebElement createanadress_button;

    @FindBy(css="div[class='choice-adress-checkout']")
    public static WebElement adresscheckout_box;

    @FindBy(css="ul[class='list-shipping-methods form-group form-group-checkradio']")
    public static WebElement shippingMethods_box;

    @FindBy(css="td[class='price']")
    public static WebElement totalTtc_box;

    @FindBy(css="p[class='links grey']")
    public static WebElement modifyDeliveryAdress_link;

    @FindBy(id="address_AddressName")
    public static WebElement modifyDeliveryAdressName_field;

    @FindBy(id="address_CompanyName")
    public static WebElement modifyDeliveryAdressCompagnyName_field;

    @FindBy(id="address_Address1")
    public static WebElement modifyDeliveryAdress1_field;

    @FindBy(id="address_PostalCode")
    public static WebElement modifyDeliveryPostalCode_field;

    @FindBy(id="address_City")
    public static WebElement modifyDeliveryCity_field;

    @FindBy(css="a[class='links grey fright']")
    public static WebElement modifyDeliveryContact_field;

    @FindBy(xpath="//input[contains(@id, 'FirstName')")
    public static WebElement modifyDeliveryContactFirstName_field;

    @FindBy(id="ContactAddressForm_new_LastName")
    public static WebElement modifyDeliveryContactLastName_field;

    @FindBy(id="ContactAddressForm_new_Email")
    public static WebElement modifyDeliveryContactEmail_field;

    @FindBy(id="ContactAddressForm_new_PhoneMobile")
    public static WebElement modifyDeliveryContactPhoneHome_field;

    @FindBy(id="headingContact_new")
    public static WebElement modifyDeliveryAddContact_link;

    @FindBy(id="ContactAddressForm_new_FirstName")
    public static WebElement modifyDeliveryNewContactFirstName_field;

    @FindBy(id="ContactAddressForm_new_LastName")
    public static WebElement modifyDeliveryNewContactLastName_field;

    @FindBy(id="ContactAddressForm_new_Email")
    public static WebElement modifyDeliveryNewContactEmail_field;

    @FindBy(id="ContactAddressForm_new_PhoneMobile")
    public static WebElement modifyDeliveryNewContactPhoneMobile_field;

    @FindBy(name="update")
    public static WebElement modifyDeliveryUpdate_button;

    @FindBy(css="div[class='message__title success above']")
    public static WebElement modalgreen_icon;

    //************************************************** Action Words **************************************************


    public void LivraisonAction() {
        debutOrder_button.click();
        Log.info("clique sur le bouton finaliser la commande");
    }
    public void ModifyDelivry()
    {

        modifyDeliveryAdress_link.click();
        modifyDeliveryAdressName_field.clear();
        modifyDeliveryAdressName_field.sendKeys("Jojo");
        modifyDeliveryAdressCompagnyName_field.clear();
        modifyDeliveryAdressCompagnyName_field.sendKeys("BillyJoe");
        modifyDeliveryAdress1_field.clear();
        modifyDeliveryAdress1_field.sendKeys("17 rue breguet");
        modifyDeliveryPostalCode_field.clear();
        modifyDeliveryPostalCode_field.sendKeys("75016");
        modifyDeliveryCity_field.clear();
        modifyDeliveryCity_field.sendKeys("Paris");
        modifyDeliveryContact_field.click();


       /* modifyDeliveryContactFirstName_field.clear();
        modifyDeliveryContactFirstName_field.sendKeys("Le magnifique");
        modifyDeliveryContactLastName_field.clear();
        modifyDeliveryContactLastName_field.sendKeys("plus beau");
        modifyDeliveryContactEmail_field.clear();
        modifyDeliveryContactEmail_field.sendKeys("test@tototest.com");
        modifyDeliveryContactPhoneHome_field.clear();
        modifyDeliveryContactPhoneHome_field.sendKeys("0606060606");
        modifyDeliveryAddContact_link.click();
        modifyDeliveryNewContactFirstName_field.clear();
        modifyDeliveryNewContactFirstName_field.sendKeys("Raja");
        modifyDeliveryNewContactLastName_field.clear();
        modifyDeliveryNewContactLastName_field.sendKeys("Rajatest");
        modifyDeliveryNewContactEmail_field.clear();
        modifyDeliveryNewContactEmail_field.sendKeys("jaiunnouvelemail@testraja.fr");
        modifyDeliveryNewContactPhoneMobile_field.clear();
        modifyDeliveryNewContactPhoneMobile_field.sendKeys("0606060620");*/
        modifyDeliveryUpdate_button.click();
    }

    public void Assert_livraisonPage ()
    {
        createanadress_button.isDisplayed();
        adresscheckout_box.isDisplayed();
        shippingMethods_box.isDisplayed();
        totalTtc_box.isDisplayed();

    }

    public void assertModify_Succes()
    {
        modalgreen_icon.isDisplayed();
        debutOrder_button.submit();

    }
}