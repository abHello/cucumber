package pageobjects;


import org.junit.Assert;
import static step_definitions.Login.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import helpers.Log;


import java.util.List;


public class LoginPage extends BaseClass {

	private String colorwarning;
	private String colorwarning_background;
	private String colorwarning_txt;
	private String emailsent_txt;


	public LoginPage(WebDriver driver) {
		super(driver);
	}

	//************************************************** WebElements **************************************************	

	@FindBy(css ="div[class='alert alert-danger']")
	public static WebElement login_button;

	@FindBy(css ="div[class='quickaccess quickaccess__account']")
	public static WebElement accountAccess_button;

	@FindBy(css ="a[class='btns red']")
	public static WebElement account_button;


	@FindBy(id="ShopLoginForm_Login")
	public static WebElement username_field;
	
	@FindBy(id="ShopLoginForm_Password")
	public static WebElement password_field;
	
	@FindBy(css=".btns")
	public static WebElement signInConfirm_btn;

	@FindBy(id="ForgotPasswordStep1Email_Login")
	public static WebElement passwordrecovery_field;

	@FindBy(css ="button[type='submit'][class='btns arrowr red']")
	public static WebElement passwordrecovery_submit_btn;

	@FindBy(css ="a[class='links grey']")
	public static List<WebElement> passwordrecovery_link;

	@FindBy(css ="div[class='alert alert-danger']")
	public static WebElement passwordrecovery_warning;

	@FindBy(css ="div[class='modal__title ']")
	public static WebElement passwordrecovery_modal_emailsent;

	@FindBy(css ="div[class='text-medium m-b-30']")
	public static WebElement passwordrecovery_modal_txtemailsent;
	
	@FindBy(css =".quickaccess__account .quickaccess__btn")
	public static WebElement myAccount_btn;
	
	//@FindBy(css="a[href*='ViewAccountIdentification']")
	@FindBy(css ="a[class='btns red']")
	public static WebElement signIn_btn;

	@FindBy(css ="div[class='sign--in text-center']")
	public static List<WebElement> signIn_Modal;

	@FindBy(css ="a[class='btns red arrowr']")
	public static WebElement signIn_popUp_button;

	@FindBy(css ="a[class='btns bdrred arrowr']")
	public static WebElement signUp_popUp_button;

	@FindBy(css ="span[class='title']")
	public static List<WebElement> titlebutton_Modal;
	
	@FindBy(css =".sign--up .btns")
	public static WebElement createAccount_btn;



	//************************************************** Action Words **************************************************
	
	public void SignInAccess() {
		myAccount_btn.click();
		signIn_btn.isDisplayed();
		signIn_btn.click();
	}

	public void SignUpAccess(){
		accountAccess_button.click();
		account_button.click();
	}


	public void LoginAction(String email, String Password) {	
		username_field.sendKeys(email);
		Log.info("entrer le login");
		password_field.sendKeys(Password);
		Log.info("entrer le password");
		signInConfirm_btn.click();
		//Reporter.log("User connecté");
	}


	public void PasswordRecoveryAction(String email) {

		passwordrecovery_link.get(0).click();
		Log.info("cliquer sur le lien mot de passe oublié");
		passwordrecovery_field.sendKeys(email);
		Log.info("entrer l'adresse email");
		passwordrecovery_submit_btn.click();
		//Reporter.log("User connecté");
	}

	public void AssertWarning() {
		colorwarning= passwordrecovery_warning.getCssValue("color").trim();
		colorwarning_background= passwordrecovery_warning.getCssValue("background-color").trim();
		colorwarning_txt= passwordrecovery_warning.getText();
		Assert.assertEquals("colorwarning should equal to: ", "rgb(174, 44, 44)", colorwarning);
		Assert.assertEquals("colorwarning_background should equal to: ", "rgba(253, 222, 224, 1)", colorwarning_background);
		Assert.assertEquals("colorwarning_txt should equal to: ", "Cet adresse email est introuvable. Nous vous invitions à créer un compte.", colorwarning_txt);
	}


	public void GrantedAccess()
	{
		passwordrecovery_modal_emailsent.isDisplayed();
		emailsent_txt=passwordrecovery_modal_txtemailsent.getText();
		Assert.assertEquals("emailsent_txt should equal to: ","Un email vient d'être envoyé à l'adresse :\n" +
		(Email.toString())+".",emailsent_txt);
	}

	public void AssertPopUpLogin()
	{
		signIn_Modal.get(0).isDisplayed();
		//signIn_Modal.get(1).isDisplayed();
		signIn_popUp_button.isDisplayed();
		signUp_popUp_button.isDisplayed();
	}
	
	public void CreateAccountAccess() {
				createAccount_btn.click();
			}
}