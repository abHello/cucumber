package pageobjects;

import static org.testng.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;


import org.jsoup.Jsoup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import helpers.propertieshelpers;
import step_definitions.Hooks;


public class RajaMailBoxPage extends BaseClass {
    WebDriver driver;
    int hour1,minute1;
    Calendar cal;
    String chanel, subject;

    public RajaMailBoxPage(WebDriver driver) {
        super(driver);
        this.driver=Hooks.driver;
    }

    @FindBy(css="#username")
    public static WebElement username;

    @FindBy(css="#password")
    public static WebElement password;

    @FindBy(css=".btn")
    public static WebElement submit_btn;

    //@FindBy(css=".nowrap taR c3")
    @FindBy(xpath="//div[@class='nowrap taR c3']/span")
    public static WebElement receivingEmailTime;

    @FindBy(css="#divSubject")
    public static WebElement emailSubject;

    @FindBy(css="#divTo")
    public static WebElement emailTo;


    public void ConnectToRajaMailBox(String userName, String passWord) {
        driver.get(propertieshelpers.getProperty("mail.url"));
        username.sendKeys(userName);
        password.sendKeys(passWord);
        submit_btn.click();
    }


    public void assertEmailReceived(String emailDestination) throws ParseException {
        cal = Calendar.getInstance();
        hour1 = cal.get(Calendar.HOUR_OF_DAY);
        minute1 = cal.get(Calendar.MINUTE);

        if(!(TimeComparator(hour1+":"+minute1, receivingEmailTime.getText()).equals(receivingEmailTime.getText()) && (emailTo.getText().equals(emailDestination)))) {
            assertTrue(false, "No email was received after test execution.");
        }
    }

    public String TimeComparator(String time1, String time2) throws ParseException {
        if(!(time2.contains(":")) || (time2.length()>5)) {
            time2="00:00";
        }
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        Date time11 = parser.parse(time1);
        Date time22 = parser.parse(time2);
        if (time11.after(time22) ) {
            return time1;
        }else {
            return time2;
        }
    }


    public boolean emailExistInBody(String email) throws Exception {
        //blocage à ce point : DS ne veulent pas activé l'imap sur la boite mail.
        boolean bl=false;
        String body = null;

        Folder inbox = null;
        Store store = null;
        try {
            Session session = Session.getDefaultInstance(new Properties( ));
            store = session.getStore("imaps");
            //store.connect("imap.datasolution.fr", 993, "qualiterajav7", "RajaDS");
            store.connect("imap.gmail.com", 993, "master.rifi2", "quicksend20");
            inbox = store.getFolder( "INBOX" );
            inbox.open( Folder.READ_WRITE );
            int mailLength=inbox.getMessageCount();
            Message messages = inbox.getMessage(mailLength);

            if (messages.isMimeType("text/plain")){

                body= messages.getContent().toString();
                if(body.indexOf(email)>0) bl=true;
            }else if (messages.isMimeType("multipart/*")) {
                String result = "";
                MimeMultipart mimeMultipart = (MimeMultipart)messages.getContent();
                int count = mimeMultipart.getCount();
                for (int i = 0; i < count; i ++){
                    BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                    if (bodyPart.isMimeType("text/plain")){
                        result = result + "\n" + bodyPart.getContent();
                        if(result.indexOf(email)>0) bl=true;
                        break;
                    } else if (bodyPart.isMimeType("text/html")){
                        String html = (String) bodyPart.getContent();
                        result = result + "\n" + Jsoup.parse(html).text();
                        if(result.indexOf(email)>0) bl=true;
                        //System.out.println("content  is : "+result);

                    }
                }}
            int messageCount = inbox.getMessageCount();
            System.out.println("Total Messages:- " + messageCount);
            inbox.close(true);
            store.close();
        } catch (Exception e) {
            throw(e);
        }
        return bl;

    }

    public boolean emailExistInRecepients(String email) throws MessagingException {
        boolean bl=false;
        Address[] header;
        Folder inbox = null;
        Store store = null;
        Session session = Session.getDefaultInstance(new Properties( ));
        store = session.getStore("imaps");
        //store.connect("imap.datasolution.fr", 993, "qualiterajav7", "RajaDS");
        store.connect("imap.gmail.com", 993, "master.rifi2", "quicksend20");
        inbox = store.getFolder( "INBOX" );
        inbox.open( Folder.READ_WRITE );

        int mailLength=inbox.getMessageCount();
        Message messages = inbox.getMessage(mailLength);
        header=messages.getAllRecipients();
        for(int i=0;i<header.length;i++) {
            if(header[i].toString().equals(email)) bl=true;
        }
        return bl;
    }

}