package pageobjects;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import helpers.propertieshelpers;
import step_definitions.Hooks;

public class SubscribePage extends BaseClass{
	private WebDriver driver;
	private String chanelURL, chanel;
	Select select;


	public SubscribePage(WebDriver driver){
		super(driver);
		this.driver=Hooks.driver;
	}


	//************************************************** WebElements **************************************************
	@FindBy(id="RegistrationForm_Email")
	public static WebElement email;

	@FindBy(id="RegistrationForm_Password")
	public static WebElement password;

	@FindBy(id="RegistrationForm_CompanyName")
	public static WebElement companyName;

	@FindBy(id="RegistrationForm_SIRET")
	public static WebElement siret;

	@FindBy(id="RegistrationForm_Title")
	public static WebElement civility_select;

	@FindBy(id="RegistrationForm_FirstName")
	public static WebElement firstName;

	@FindBy(id="RegistrationForm_LastName")
	public static WebElement lastName;

	@FindBy(id="RegistrationForm_PhoneHome")
	public static WebElement phone;

	@FindBy(id="RegistrationForm_Service")
	public static WebElement service_select;

	@FindBy(id="RegistrationForm_Address1")
	public static WebElement address;

	@FindBy(id="RegistrationForm_PostalCode")
	public static WebElement postalCode;

	@FindBy(id="RegistrationForm_City")
	public static WebElement city;

	@FindBy(id="RegistrationForm_CountryCode_chosen")
	public static WebElement country;

	@FindBy(id="RegistrationForm_FISCAL_CODE")
	public static WebElement fiscalCode;

	@FindBy(id="RegistrationForm_TVA_NUMBER")
	public static WebElement tvaNumber;

	@FindBy(id="RegistrationForm_County")
	public static WebElement provincia;

	@FindBy(css=".form-group.form-group-checkbox .icheck_line-icon")
	public static WebElement AcceptPrivacyPolicy_submit;

	@FindBy(id="registerSubmit")
	public static WebElement register_submit;

	@FindBy(xpath="//*[@id=\"RegistrationForm\"]/div[2]/div[2]/div[1]/div/div")
	public static WebElement entreprise_checkbox;

	@FindBy(xpath="//*[@id=\"RegistrationForm\"]/div[2]/div[2]/div[2]/div/div")
	public static WebElement individual_checkbox;
	

	//************************************************** Action Words **************************************************
	public void register(String userEmail, String organisation, String userPostalCode) {
		
		/************************ Define Launched channel ************************/
		chanelURL=driver.getCurrentUrl();
		if(chanelURL.contains("FR")) chanel="fr";
		else if(chanelURL.contains("IT")) chanel="it";
		else if(chanelURL.contains("GB")) chanel="uk";
		else if(chanelURL.contains("ES")) chanel="es";
		else if(chanelURL.contains("DE")) chanel="de";
		else if(chanelURL.contains("BE")) chanel="be";
		/*************************************************************************/
		
		/***************************** Common fields *****************************/
		email.sendKeys(userEmail);
		password.sendKeys(propertieshelpers.getProperty("subscribe.password"));
		firstName.sendKeys(propertieshelpers.getProperty("firstName"));
		lastName.sendKeys(propertieshelpers.getProperty("lastName"));
		address.sendKeys(propertieshelpers.getProperty("address"));
		Actions actions = new Actions(driver);
		/*************************************************************************/
		
		/*************** Switch registration based on channel *********************/
		switch (chanel) {
			case "fr":
				if(organisation.equalsIgnoreCase("entreprise")) {
					actions.moveToElement(entreprise_checkbox).click().build().perform();
					companyName.sendKeys(propertieshelpers.getProperty("companyName"));
				}else {
					actions.moveToElement(individual_checkbox).click().build().perform();
					individual_checkbox.click();
				}
				select=new Select(civility_select);
				select.selectByIndex(1);
				phone.sendKeys(propertieshelpers.getProperty("phone.fr"));
				postalCode.sendKeys(userPostalCode);
				city.sendKeys(propertieshelpers.getProperty("city"));
				break;

			case "it":
				if(organisation.equalsIgnoreCase("entreprise")) {
					actions.moveToElement(entreprise_checkbox).click().build().perform();
					tvaNumber.sendKeys(propertieshelpers.getProperty("tva.it"));
				} else {
					actions.moveToElement(individual_checkbox).click().build().perform();
				}
				companyName.sendKeys(propertieshelpers.getProperty("companyName"));
				fiscalCode.sendKeys(propertieshelpers.getProperty("fiscalCode.it"));
				phone.sendKeys(propertieshelpers.getProperty("phone.it"));
				postalCode.sendKeys(userPostalCode);
				city.sendKeys(propertieshelpers.getProperty("city"));
				provincia.sendKeys(propertieshelpers.getProperty("provincia.it"));
				actions.moveToElement(AcceptPrivacyPolicy_submit).click().build().perform();
				break;
				
			case "uk":
				select=new Select(civility_select);
				select.selectByIndex(1);
				phone.sendKeys(propertieshelpers.getProperty("phone.uk"));
				companyName.sendKeys(propertieshelpers.getProperty("companyName"));
				city.sendKeys(propertieshelpers.getProperty("city"));
				postalCode.sendKeys(userPostalCode);
				break;
				
			case "be":
				select=new Select(civility_select);
				select.selectByIndex(1);
				phone.sendKeys(propertieshelpers.getProperty("phone.be"));
				companyName.sendKeys(propertieshelpers.getProperty("companyName"));
				tvaNumber.sendKeys(propertieshelpers.getProperty("tva.be"));
				postalCode.sendKeys(userPostalCode);
				city.sendKeys(propertieshelpers.getProperty("city"));
				actions.moveToElement(AcceptPrivacyPolicy_submit).click().build().perform();
				break;
				
			case "de":
				
		}
		/****************************************************************************/
		register_submit.click();
	}

	public void assert_account_is_created() {
		assertTrue(driver.getCurrentUrl().contains("AccountCreation=true"), "Création de compte échouée sur "+chanel);
	}



}