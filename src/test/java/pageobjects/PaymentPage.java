package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import helpers.Log;

import java.util.List;

public class PaymentPage extends BaseClass{

    public PaymentPage(WebDriver driver) {
        super(driver);
    }

    //************************************************** WebElements **************************************************

    @FindBy(xpath="//*[@id=\"formPayment\"]/div/div[2]/ul")
    public static WebElement paymentchoice_Block;

    @FindBy(xpath="//*[@id=\"formPayment\"]/div/div[2]/div[5]/div[1]/div/div")
    public static WebElement PaymentCgu_Checkbox;

    @FindBy(xpath="//*[@id=\"formPayment\"]/div/div[2]/div[5]/div[2]/button")
    public static WebElement Payment_button;

    @FindBy(css="button[class='btns red arrowr']")
    public static WebElement debutPayment_button;

    @FindBy(css="div[class='choice-adress-checkout']")
    public static WebElement checkoutAdress_box;

    @FindBy(css="button[class='checkout-commande collapsed']")
    public static WebElement checkoutcommande_box;

    @FindBy(css="div[class='m-t-10']")
    public static WebElement recapDelivery_form;

    @FindBy(css="div[class='m-b-15']")
    public static WebElement recapBilling_form;

    @FindBy(css="div[class='m-b-20 m-t-20']")
    public static WebElement recapContact_form;

    @FindBy(css="div[class='m-b-10 m-t-10']")
    public static WebElement recapDeliveryMethod_form;

    @FindBy(css="div[class='checkout__aside scroll-to-fixed-fixed']")
    public static WebElement recapOrder_box;

    @FindBy(id="Ecom_Payment_Card_Number")
    public static WebElement numberCard_field;

    @FindBy(id="Ecom_Payment_Card_ExpDate_Month")
    public static WebElement monthCard_field;

    @FindBy(id="Ecom_Payment_Card_ExpDate_Year")
    public static WebElement yearCard_field;

    @FindBy(id="Ecom_Payment_Card_Verification")
    public static WebElement csvCard_field;

    @FindBy(id="submit3")
    public static WebElement confirmPayment_button;

    @FindBy(id="btn_Back")
    public static WebElement returnPayment_button;

    @FindBy(id="storealias")
    public static WebElement ingenico_checkbox;

    @FindBy(id="btn_Accept")
    public static WebElement paypalvalidpayment_button;
    //************************************************** Action Words **************************************************
    public void paymentMethod (int i)
    {
        List<WebElement> lst = paymentchoice_Block.findElements(By.tagName("li"));

        lst.get(i).click();
    }

    public void PaymentAction() {
        debutPayment_button.click();
        checkoutcommande_box.click();
    }
    public void PaymentChoiceAndDo() throws InterruptedException {
        Actions actions = new Actions(driver);
       // actions.moveToElement(paymentchoice_Block).click().build().perform();
        JavascriptExecutor js = (JavascriptExecutor) driver;

        String javas = "arguments[0].style.height='auto'; arguments[0].style.visibility='visible';";

      paymentMethod(0);

        Log.info("clique sur CB");

        actions.moveToElement(PaymentCgu_Checkbox).click().build().perform();
        Payment_button.click();
        Log.info("clique sur le bouton finaliser le paiement");
    }

    public void insertCardNumber(String number, String csv)
        {ingenico_checkbox.click();
        numberCard_field.sendKeys(number);
        monthCard_field.sendKeys("12");
        yearCard_field.sendKeys("2019");
        csvCard_field.sendKeys(csv);
        confirmPayment_button.click();
    }

    public void AssertPaymentPage()
    {
        recapBilling_form.isDisplayed();
        recapContact_form.isDisplayed();
        recapDelivery_form.isDisplayed();
        recapDeliveryMethod_form.isDisplayed();
        recapOrder_box.isDisplayed();
        checkoutAdress_box.isDisplayed();
    }


    public void PaymentTransfer()
    {
        Actions actions = new Actions(driver);
        paymentMethod(2);
        Log.info("clique sur virement");
        actions.moveToElement(PaymentCgu_Checkbox).click().build().perform();
        Payment_button.click();
        Log.info("clique sur le bouton finaliser le paiement");
    }

    public void PaymentBankCheck()
    {
        Actions actions = new Actions(driver);
        paymentMethod(3);
        Log.info("clique sur Cheque");
        actions.moveToElement(PaymentCgu_Checkbox).click().build().perform();
        Payment_button.click();
        Log.info("clique sur le bouton finaliser le paiement");
    }

    public void PaymentPaypal()
    {
        Actions actions = new Actions(driver);
        paymentMethod(4);
        Log.info("clique sur Paypal");
        actions.moveToElement(PaymentCgu_Checkbox).click().build().perform();
        Payment_button.click();
        Log.info("clique sur le bouton finaliser le paiement");
        paypalvalidpayment_button.click();
    }
}
