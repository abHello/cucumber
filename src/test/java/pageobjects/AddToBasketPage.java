package pageobjects;


import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AddToBasketPage extends BaseClass {
	
	private static List<WebElement> UsersNumber; 

	public AddToBasketPage(WebDriver driver) {	
		super(driver);
	}
	
	//************************************************** WebElements **************************************************		
	@FindBy(xpath="//div[@class='quickaccess quickaccess__reference']/div")
	public static WebElement orderByReference_btn;
	
	@FindBy(css=".search__product-by-ref")
	public static WebElement productRef_field;
	
	@FindBy(css=".btns")
	public static WebElement addToBasket_btn;
	
	@FindBy(css=".tt-suggestion.tt-selectable table")
	public static WebElement TableProduct;
	
	//@FindBy(css="a[href^='ViewCart-View']")
	@FindBy(xpath="//a[@class='btns bdrred arrowr']")
	public static WebElement ViewBasket_btn;
	
	@FindBy(css=".checkout-panier-table")
	public static WebElement CheckoutBasket_table;
	
	
	//************************************************** Action Words **************************************************	
	public void order_by_reference() {
		orderByReference_btn.click();
	}
	
	public void enter_product_reference(String product_ref) {
		productRef_field.sendKeys(product_ref);
		productRef_field.click();
	}
	
	public void Select_product_from_list() {
		UsersNumber=TableProduct.findElements(By.tagName("td"));
		UsersNumber.get(1).click();
	}
	
	
	public void add_product_to_basket() {
		addToBasket_btn.click();
	}
	
	public void view_cart() {
		ViewBasket_btn.click();
	}
	
	public void verify_product_exist_in_basket() {
		assertTrue(CheckoutBasket_table.isDisplayed());
	}	
		
	public void add_article_to_basket(String product_ref) {
				orderByReference_btn.click();
				productRef_field.sendKeys(product_ref);
				productRef_field.click();
				UsersNumber=TableProduct.findElements(By.tagName("td"));
				UsersNumber.get(0).click();
				addToBasket_btn.click();
				ViewBasket_btn.click();
			}
}
