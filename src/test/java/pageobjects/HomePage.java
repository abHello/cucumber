package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import helpers.Log;
import step_definitions.Hooks;

public class HomePage extends BaseClass {
	private WebDriver driver;
	String chanelURL;

	public HomePage(WebDriver driver) {
		super(driver);
		this.driver=Hooks.driver;
	}

	//************************************************** WebElements **************************************************
	@FindBy(css=".quickaccess__account .quickaccess__btn")
	public static WebElement myAccount_btn;
	
	@FindBy(css="a[href*='ViewAccountIdentification']")
	public static WebElement signIn_btn;

	@FindBy(css =".account__bottom .links")
	public static WebElement register_link;
	
	@FindBy(css=".kamPopup__close")
	public static WebElement popup_fr;
	
	@FindBy(css=".js_modal-close")
	public static WebElement LanguageModal_be;
	
	//************************************************** Action Words **************************************************	
	public void LoginPageAccessAction() {
		
		myAccount_btn.click();
		Log.info("cliquer sur le bouton 'mon compte'.");
		
		signIn_btn.click();
		Log.info("cliquer sur le bouton 'se connecter'.");
	}

	public void RegisterPageAccess() {
				myAccount_btn.click();
				register_link.click();
		 }
	
	
	public void getToURL(String url) {		
		driver.get(url);
		chanelURL=driver.getCurrentUrl();
		if(chanelURL.contains("be")) LanguageModal_be.click();
		else if(chanelURL.contains("fr")) popup_fr.click();
	}

}
