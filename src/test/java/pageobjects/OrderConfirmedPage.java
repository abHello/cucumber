package pageobjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import helpers.Log;

import java.util.List;
public class OrderConfirmedPage extends BaseClass{
    public OrderConfirmedPage(WebDriver driver) {
        super(driver);
    }

    //************************************************** WebElements **************************************************
    @FindBy(css="i[class='icon icon-raja-check-circle']")
    public static WebElement iconCheck;

    @FindBy(css="div[class='confirmation-number box__confirmation']")
    public static WebElement confirmationBox;


    @FindBy(css="div[class='choice-adress-checkout']")
    public static List<WebElement> confirmationAdress_Box;

    //************************************************** Action Words **************************************************

    public void AssertOrderConfirmed() {
        iconCheck.isDisplayed();
        confirmationBox.isDisplayed();
        int size = confirmationAdress_Box.size();
        size =2;
    }
}
