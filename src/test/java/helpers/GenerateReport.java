package helpers;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;

public class GenerateReport {
    public static void main(String[] args) {
        File reportOutputDirectory = new File("target/cucumber-parallel/chrome");
        List<String> jsonFiles = new ArrayList<String>();
        //jsonFiles.add("target/cucumber-reports/cucumber.json");
        jsonFiles.add("target/cucumber-parallel/chrome.json");
        jsonFiles.add("target/cucumber-parallel/safari.json");

        String buildNumber = "2";
        String projectName = "cucumberProject";
        boolean runWithJenkins = false;

        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        // optional configuration - check javadoc
        configuration.setRunWithJenkins(runWithJenkins);
        configuration.setBuildNumber(buildNumber);
        System.out.println("Report file path is : "+configuration.getReportDirectory());

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        Reportable result = reportBuilder.generateReports();
        // if report has failed features, undefined steps etc
    }
    
    public static void Report() {
        File reportOutputDirectory = new File("target/cucumber-parallel/chrome");
        List<String> jsonFiles = new ArrayList<String>();
        //jsonFiles.add("target/cucumber-reports/cucumber.json");
        //jsonFiles.add("target/cucumber-parallel/chrome.json");
        jsonFiles.add("target/cucumber-parallel/safari.json");

        String buildNumber = "2";
        String projectName = "cucumberProject";
        boolean runWithJenkins = false;

        Configuration configuration = new Configuration(reportOutputDirectory, projectName);
        // optional configuration - check javadoc
        configuration.setRunWithJenkins(runWithJenkins);
        configuration.setBuildNumber(buildNumber);
        System.out.println("Report file path is : "+configuration.getReportDirectory());

        // addidtional metadata presented on main page

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        Reportable result = reportBuilder.generateReports();
        result.getSteps();
        // and here validate 'result' to decide what to do
    }
}

