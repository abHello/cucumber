Feature: Je souhaite pouvoir ajouter des articles au panier

  	Scenario Outline: Je souhaite ajouter des articles au panier en étant déconnecté du compte Raja
    	Given Un utilisateur non authentifié se rendant dans la page de commande par référence "<url>"
    	When l'utilisateur ajoute un article "<reference_prod>" dans son panier
    	Then l'article est présent dans son panier
 
Examples:
|url|reference_prod|
|home.url.fr|reference_produit|
|home.url.it|reference_produit|


Scenario Outline: Je souhaite ajouter des articles au panier en étant connecté au compte Raja
    Given Un utilisateur authentifié se rendant dans le site Raja "<url>"
    When  l'utilisateur ajoute un article "<reference_prod>" dans son panier
    Then  l'article est présent dans son panier
 
Examples:
|url|reference_prod|
|home.url.fr|reference_produit|
|home.url.fr|reference_produit|
    