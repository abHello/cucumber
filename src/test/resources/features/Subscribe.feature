Feature: Je souhaite pouvoir créer mon compte
@test
  Scenario Outline: Je souhaite créer mon compte depuis la home
    Given Un utilisateur se rendant sur le site Raja "<url>"
    When l'utilisateur choisit de créer son compte "<organisation>", "<postalCode>"
    Then le compte est crée
    And un email de confirmation est envoyé au client
    And un email de confirmation est envoyé au CRC

    Examples:
      |url|organisation|postalCode|
      |home.url.fr|organisation|postalCode.fr|
      |home.url.it|organisation|postalCode.it|
      |home.url.uk|organisation|postalCode.uk|
      |home.url.be|organisation|postalCode.be|
      
      

   Scenario Outline: Je souhaite créer mon compte depuis le tunnel d'achat
    Given Un utilisateur se rendant sur le site Raja "<url>" et ayant des articles dans son panier
    When l'utilisateur choisit de créer son compte "<organisation>", "<postalCode>" pour valider son panier
    Then le compte est crée
    And un email de confirmation est envoyé au client
    And un email de confirmation est envoyé au CRC
    
    Examples:
      |url|organisation|postalCode|
      |home.url.fr|organisation|postalCode.fr|
      |home.url.it|organisation|postalCode.it|
      |home.url.uk|organisation|postalCode.uk|
      |home.url.be|organisation|postalCode.be|
   