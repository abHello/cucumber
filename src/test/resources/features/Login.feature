Feature: Je souhaite pouvoir me connecter au site Raja

  Scenario Outline: Je souhaite me connecter avec un utilisateur valide et un mot de passe valide
    Given Un user se rendant sur le site d'authentification de Raja "<url>"
    When Je me connecte avec username "<username>" et password "<password>"
    Then je suis connecté

Examples:
|url|username|password|
|home.url.fr|username.login.valid|username.password.valid|
|home.url.it|username.login.valid|username.password.valid|

 Scenario Outline: Je souhaite me connecter avec un utilisateur valide et un mot de passe invalide
    Given Un user se rendant sur le site d'authentification de Raja "<url>"
    When Je me connecte avec username "<username>" et password "<password>"
    Then je ne suis pas connecté

Examples:
|url|username|password|
|home.url.fr|username.login.valid|username.password.invalid|
|home.url.fr|username.login.invalid|username.password.valid|
|home.url.it|username.login.valid|username.password.invalid|
|home.url.it|username.login.invalid|username.password.valid|

  Scenario Outline: Je souhaite utiliser la fonction rappel mot de passe de mon compte en entrant un email inconnu en base
    Given Un user se rendant sur le site d'authentification de Raja "<url>"
    When Je rentre le compte "<username>"
    Then Une modal d'erreur s'affiche

    Examples:
      |url|username|
      |login.url.fr|username.passwordrecovery.invalid|


  Scenario Outline: Je souhaite utiliser la fonction rappel mot de passe de mon compte en entrant un email connu en base
    Given Un user se rendant sur le site d'authentification de Raja "<url>"
    When Je rentre le compte "<username>"
    Then Une modal d'envoie s'affiche

    Examples:
      |url|username|
      |login.url.fr|username.passwordrecovery.valid|