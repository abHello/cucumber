Feature: Je souhaite realiser le paiment de ma commande

  Scenario Outline: Je souhaite payer ma commande
    Given un client authentifié sur "<url>" ayant des articles dans son panier et ayant fait son choix du mode de livraison
    When je souhaite effectuer le paiment de ma commande
    Then la page de paiement s’affiche avec l'adresse de facturation et il est possible de choisir un mode de paiement

    Examples:
      |url|
      |delivery.url.fr|


  Scenario Outline: Je paie ma commande avec ma CB
    Given un client authentifié sur "<url>" se situant sur la page de paiement et ayant accepter les CGV
    When je souhaite effectuer le paiment de ma commande avec ma carte bancaire déjà enregistrée
    Then la commande est passée

    Examples:
      |url|
      |delivery.url.fr|


  Scenario Outline: Je paie ma commande par virement
    Given un client authentifié sur "<url>" se situant sur la page de paiement et ayant accepter les CGV
    When je souhaite effectuer le paiment de ma commande par virement
    Then la commande est passée

    Examples:
      |url|
      |delivery.url.fr|


  Scenario Outline: Je paie ma commande par virement
    Given un client authentifié sur "<url>" se situant sur la page de paiement et ayant accepter les CGV
    When je souhaite effectuer le paiment de ma commande par cheque
    Then la commande est passée

    Examples:
      |url|
      |delivery.url.fr|


  Scenario Outline: Je paie ma commande par paypal
    Given un client authentifié sur "<url>" se situant sur la page de paiement et ayant accepter les CGV
    When je souhaite effectuer le paiment de ma commande par paypal
    Then la commande est passée

    Examples:
      |url|
      |delivery.url.fr|