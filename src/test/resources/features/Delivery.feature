Feature: Je souhaite finaliser ma commande 

  Scenario Outline: Je souhaite attérir dans la page d'authentification lorsque je finalise ma commande en étant déconnecté
    Given un client non authentifié sur "<url>" et ayant des articles dans son panier
    When je souhaite finaliser ma commande
    Then la fenêtre d’authentification apparait proposant deux choix : l’authentification et l’inscription.

    Examples:
      |url|
      |delivery.url.fr|
      |delivery.url.it|


  Scenario Outline:Je suis un client authentifié et je souhaite finaliser ma commande
    Given un client authentifié sur "<url>" et ayant des articles dans son panier
    When je souhaite finaliser ma commande
    Then la fenêtre de livraison apparait avec : les informations du client  le tableau récapitulatif des articles, et le choix du mode de livraison

    Examples:
      |url|
      |delivery.url.fr|
      |delivery.url.it|



  Scenario Outline:Je suis un nouveau client inscrit et je souhaite modifier mon adresse de livraison lors du choix du mon de livraison
    Given un client s'inscrit sur "<url>" et ayant des articles dans son panier et ayant fait son choix de mode de livraison
    When je souhaite modifier l'adresse de livraison
    Then la page est modifiée
    Examples:
      |url|
      |delivery.url.fr|